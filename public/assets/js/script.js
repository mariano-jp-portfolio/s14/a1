const inputToDo = document.querySelector('#toDo');
const btnAdd = document.querySelector('#add');
const btnClear = document.querySelector('#clear');
const list = document.querySelector('#list');
const arrayLength = document.querySelector('#array-length');
const firstEntry = document.querySelector('#first-entry');
const latestEntry = document.querySelector('#latest-entry');
const stretch = document.querySelector('#stretch');

let toDoArray = [];

// Event listener to input the to do when btnAdd is clicked
btnAdd.onclick = () => {
	if (inputToDo.value === "") {
		alert(`Enter a task first!`);
	} else {
		let newListItem = document.createElement('li');
		newListItem.textContent = inputToDo.value;
		list.appendChild(newListItem);
	
		toDoArray.push(inputToDo.value);
		//console.log(toDoArray); //For checking of array
		inputToDo.value = null;
	
		//Array details
		if (toDoArray.length === 1) {
			arrayLength.textContent = `You have ${toDoArray.length} task to do.`;
		} else {
			arrayLength.textContent = `You have ${toDoArray.length} tasks to do.`;
		
			firstEntry.textContent = `First on your list is "${toDoArray[0]}".`;
	
			for (let i = 1; i < toDoArray.length; i++) {
				latestEntry.textContent = `Last on your list is "${toDoArray[i]}".`;
			}
		}
	
		//A way to cross out the user's finished tasks
		newListItem.onclick = () => {
		newListItem.classList.toggle('done-list'); //When the task is clicked again, the strikethrough will be gone
		}
		
		// Stretch Goal, can be removed in the future
		let numOfTasks = toDoArray.length;
		// console.log(numOfTasks); //For checking of array length
		
		if (numOfTasks === 5) {
			stretch.textContent = `You're doing good.`;
		} else if (numOfTasks > 5 && numOfTasks <= 10) {
			stretch.textContent = `Very good, be sure to finish them all.`;
		} else if (numOfTasks > 10) {
			stretch.textContent = `Hey take it easy! Don't forget to take a rest.`;
		} else {
			stretch.textContent = `List your to do's here.`;
		}
	}
}	
	


// Event listener when btnClear is clicked
btnClear.onclick = () => {
	inputToDo.value = null;
	toDoArray = [];
	list.textContent = null;
	arrayLength.textContent = `You have no tasks to do.`;
	firstEntry.textContent = null;
	latestEntry.textContent = null;
	stretch.textContent = null;
}